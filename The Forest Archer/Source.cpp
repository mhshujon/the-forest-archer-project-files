#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include <conio.h>
#include "iGraphics.h"

#define KEY_UP 72
#define KEY_DOWN 80
#define KEY_ECS 27
#define KEY_SPACE 32
#define KEY_ENTER 13


int blackPosition_Y=436,violetPosition_Y=447,yellowPosition_Y=459,greenPosition_Y=470,redPosition_Y=481;
int arrowPosition_X1=770,arrowPosition_X2=arrowPosition_X1+60,arrowPosition_Y=316,boardDirection=1,arrowDirection,arrow=0,score=0,totalScore=0;
int anglePosition_X1=arrowPosition_X1,anglePosition_X2=anglePosition_X1+10,angleBackPosition_X1=556,angleBackPosition_X2=560;
int anglePosition_Y1=316,anglePosition_Y2=320,anglePosition_Y3=312,display,instructionsPage=4,aboutPage=3,highScorePage=5;
int i=0,j=1,k=2,l=3,m=4,textLineY=349,textLineY1=351,textLineY2=352,selectedMenu,startPage=0,menuPage=1,gamePage=2,afterGamePage=6,exitPage1=7,exitPage2=8;
int aboutLenth=0,instructionsLenth=0,highScoreLenth=0,exitDisplay,exitX=380,selectedExit=0,selectedPlayOrExit,playORexitX=30,points,spaceSecond;
int minutes1=1,minutes2=0,minutes3=0,seconds1=60,seconds2=50,seconds3=40,difficultMode,easy=0,normal=1,hard=2,difficultModePage=9,selectedDifficult,difficult_,secondPart=1;
int num,num1,credSec=0;
char score_[4]="000",total_Score[15]="Total Score : ",scoreDisplay[3],highScore[2][4],points_[3]="0";
char minutes_1[3],minutes_2[3],minutes_3[3],seconds_1[3],seconds_2[3],seconds_3[3];

char eXIT[2][8]=
{
	">>YES<<",
	">>NO<<",
};

char difficult[3][11]=
{
	">>Easy<<",
	">>Normal<<",
	">>Hard<<",
};

char playORexit[2][24]=
{
	">>Play again<<",
	">>Return to main menu<<",
};

char MenuText[5][20]=
{
	">> Start Game <<",
	">> High Score <<",
	">> Instruction <<",
	">> Credits <<",
	">> Exit <<",
};

void CreditMove()
{
	if(display==aboutPage)
		credSec++;
}


void HighScoreCheck()
{
	if(display==gamePage)
	{
		FILE* hScore=fopen("Documents\\HighScore.txt" , "r");

		fscanf(hScore,"%d",&num);
		fclose(hScore);

		if(totalScore>num)
		{
			FILE* hScore=fopen("Documents\\HighScore.txt" , "w");

			fprintf(hScore,"%d",totalScore);
			fclose(hScore);
		}
	}

}

void pressSPACE()
{
	if(display==startPage)
		spaceSecond++;
	else
		spaceSecond=0;
}

void stopWatchEasy()
{
	if(display==gamePage)
	{
		if(difficult_==easy)
		{
			sprintf(minutes_1,"%d",minutes1);
			seconds1-=1;
			sprintf(seconds_1,"%d",seconds1);
			if(seconds1==0)
			{
				minutes1-=1;
				seconds1=60;
				sprintf(minutes_1,"%d",minutes1);
				if(minutes1==0)
				{
					display=afterGamePage;
					blackPosition_Y=436;
					violetPosition_Y=447;
					yellowPosition_Y=459;			
					greenPosition_Y=470;
					redPosition_Y=481;
					arrowDirection=0;
					arrow=0;
					arrowPosition_X1=770;
					arrowPosition_X2=arrowPosition_X1+60;
					totalScore=0;
					exitDisplay=0;
					score=0;
					minutes1=1;
					seconds1=60;
				}
			}	
		}
		else if(difficult_==normal)
		{
			sprintf(minutes_2,"%d",minutes2);
			seconds2-=1;
			sprintf(seconds_2,"%d",seconds2);
			if(seconds2==0)
			{
				display=afterGamePage;
				blackPosition_Y=436;
				violetPosition_Y=447;
				yellowPosition_Y=459;			
				greenPosition_Y=470;
				redPosition_Y=481;
				arrowDirection=0;
				arrow=0;
				arrowPosition_X1=770;
				arrowPosition_X2=arrowPosition_X1+60;
				totalScore=0;
				exitDisplay=0;
				score=0;
				minutes2=0;
				seconds2=50;
			}
		}
		else if(difficult_==hard)
		{
			sprintf(minutes_3,"%d",minutes3);
			seconds3-=1;
			sprintf(seconds_3,"%d",seconds3);
			if(seconds3==0)
			{
				display=afterGamePage;
				blackPosition_Y=436;
				violetPosition_Y=447;
				yellowPosition_Y=459;			
				greenPosition_Y=470;
				redPosition_Y=481;
				arrowDirection=0;
				arrow=0;
				arrowPosition_X1=770;
				arrowPosition_X2=arrowPosition_X1+60;
				totalScore=0;
				exitDisplay=0;
				score=0;
				minutes3=0;
				seconds3=40;
				
			}
		}
	}
	
}


void boardMovement()
{
	if(display==gamePage)
	{	
		if(boardDirection==1)
		{
			blackPosition_Y-=1;
			violetPosition_Y-=1;
			yellowPosition_Y-=1;
			greenPosition_Y-=1;
			redPosition_Y-=1;
			if(blackPosition_Y<=15)
			{
				boardDirection=2;
			}
		}

		else
		{
			blackPosition_Y+=1;
			violetPosition_Y+=1;
			yellowPosition_Y+=1;
			greenPosition_Y+=1;
			redPosition_Y+=1;

			if(blackPosition_Y>=436)
			{
				boardDirection=1;
			}
		}
	}
}

void arrowShot()
{
	if(display==gamePage)
	{
		if(arrowDirection==1)
		{
			arrowPosition_X1-=5;
			arrowPosition_X2-=5;
			anglePosition_X1-=5;
			anglePosition_X2-=5;
			if((((blackPosition_Y<=214 && blackPosition_Y>=203) || (blackPosition_Y>=305 && blackPosition_Y<=316 )) && (arrowPosition_X1==25)) || (((violetPosition_Y<=316 && violetPosition_Y>=305) || (violetPosition_Y>=226 && violetPosition_Y<=237 )) && (arrowPosition_X1==30)) || (((yellowPosition_Y<=316 && yellowPosition_Y>=305) || (yellowPosition_Y>=249 && violetPosition_Y<=260 )) && (arrowPosition_X1==35)) || (((greenPosition_Y<=316 && greenPosition_Y>=305) || (greenPosition_Y>=271 && greenPosition_Y<=282 )) && (arrowPosition_X1==40)) || (((redPosition_Y>=294 && redPosition_Y<=316 )) && (arrowPosition_X1==50)) || (arrowPosition_X1<=15))
			{
				arrowDirection=2;
			}
		
		}
	

		else
		{
			arrowPosition_X1+=1000;
			arrowPosition_X2+=1000;

			if(arrowPosition_X1>=1000)
			{
				arrowPosition_X1=770;
				arrowPosition_X2=830;
				anglePosition_X1=770;
				anglePosition_X2=780;
			}
		}

		if(((blackPosition_Y<=214 && blackPosition_Y>=203) || (blackPosition_Y>=305 && blackPosition_Y<=316 )) && (arrowPosition_X1==25))  //For black hit =10
		{
			totalScore+=10;
			score=10;
		}
		if(((violetPosition_Y<=316 && violetPosition_Y>=305) || (violetPosition_Y>=226 && violetPosition_Y<=237 )) && (arrowPosition_X1==30))  //For violet hit =20
		{
			totalScore+=20;
			score=20;
		}
		if(((yellowPosition_Y<=316 && yellowPosition_Y>=305) || (yellowPosition_Y>=249 && violetPosition_Y<=260 )) && (arrowPosition_X1==35))  //For yellow hit =30
		{
			totalScore+=30;
			score=30;
		}
		if(((greenPosition_Y<=316 && greenPosition_Y>305) || (greenPosition_Y>=271 && greenPosition_Y<=282 )) && (arrowPosition_X1==40))  //For green hit =40
		{
			totalScore+=40;
			score=40;
		}
		if(((redPosition_Y>=294 && redPosition_Y<=316 )) && (arrowPosition_X1==50))  //For green hit =50
		{
			totalScore+=50;
			score=50;
		}
		if(arrowPosition_X1<=15)
		{
			totalScore-=5;
			score=-5;
		}
		points=score;
		sprintf_s(score_,"%d",totalScore);
		sprintf_s(scoreDisplay,"%d",score);
		sprintf_s(points_,"%d",points);
		num1=totalScore;		
	}



}

void iDraw()
{
	//place your drawing codes here
	iClear();
	if(display==startPage)
	{
		iShowBMP(0,0,"Image\\CoverPage.bmp");
		iSetColor(255,255,255);
		if(spaceSecond%2==0)
			iText(365,40,"Press SPACE to continue. . .",GLUT_BITMAP_TIMES_ROMAN_24);
	}

	else if(display==menuPage)
	{
		iShowBMP(0,0,"Image\\MenuPage.bmp");

		if(i==selectedMenu)
			iSetColor(255,153,10);
		else
			iSetColor(255,255,255);
		iText(411,textLineY,MenuText[i],GLUT_BITMAP_TIMES_ROMAN_24);
		if(j==selectedMenu)
			iSetColor(255,153,10);
		else
			iSetColor(255,255,255);
		iText(411,textLineY-30,MenuText[j],GLUT_BITMAP_TIMES_ROMAN_24);
		if(k==selectedMenu)
			iSetColor(255,153,10);
		else
			iSetColor(255,255,255);
		iText(411,textLineY-60,MenuText[k],GLUT_BITMAP_TIMES_ROMAN_24);
		
		if(l==selectedMenu)
			iSetColor(255,153,10);
		else
			iSetColor(255,255,255);
		iText(435,textLineY-90,MenuText[l],GLUT_BITMAP_TIMES_ROMAN_24);
		
		if(m==selectedMenu)
			iSetColor(255,153,10);
		else
			iSetColor(255,255,255);
		iText(446,textLineY-120,MenuText[m],GLUT_BITMAP_TIMES_ROMAN_24);
		
	}
	
	else if(display==gamePage)
	{
		HighScoreCheck();
		iShowBMP(0,0,"Image\\Archery.bmp");
		iSetColor(0,0,0);	
		iText(325,424,total_Score, GLUT_BITMAP_TIMES_ROMAN_24);
		iText(455,424,score_, GLUT_BITMAP_TIMES_ROMAN_24);
		iText(350,390,"Points :", GLUT_BITMAP_TIMES_ROMAN_24);
		iText(435,390,scoreDisplay, GLUT_BITMAP_TIMES_ROMAN_24);

		if(difficult_==easy)
			iText(370,456,minutes_1, GLUT_BITMAP_TIMES_ROMAN_24);
		if(difficult_==normal)
			iText(370,456,minutes_2, GLUT_BITMAP_TIMES_ROMAN_24);
		if(difficult_==hard)
			iText(370,456,minutes_3, GLUT_BITMAP_TIMES_ROMAN_24);
		iText(387,456,":", GLUT_BITMAP_TIMES_ROMAN_24);
		if(difficult_==easy)
			iText(400,456,seconds_1, GLUT_BITMAP_TIMES_ROMAN_24);
		if(difficult_==normal)
			iText(400,456,seconds_2, GLUT_BITMAP_TIMES_ROMAN_24);
		if(difficult_==hard)
			iText(400,456,seconds_3, GLUT_BITMAP_TIMES_ROMAN_24);
	
		

		iSetColor(115,59,19);				//Brown Color
		iLine(arrowPosition_X1,arrowPosition_Y,arrowPosition_X2,arrowPosition_Y);
		iLine(anglePosition_X1,anglePosition_Y1,anglePosition_X2,anglePosition_Y2);
		iLine(anglePosition_X1,anglePosition_Y1,anglePosition_X2,anglePosition_Y3);

		if(arrow==0)
		{
			iShowBMP(755,519,"Image\\Arrow1.bmp");
			iShowBMP(755,504,"Image\\Arrow2.bmp");
			iShowBMP(755,489,"Image\\Arrow3.bmp");
			iShowBMP(755,474,"Image\\Arrow4.bmp");
			iShowBMP(755,459,"Image\\Arrow5.bmp");
			iShowBMP(755,444,"Image\\Arrow6.bmp");
		}
		if(arrow==1)
		{
			iShowBMP(755,519,"Image\\Arrow1.bmp");
			iShowBMP(755,504,"Image\\Arrow2.bmp");
			iShowBMP(755,489,"Image\\Arrow3.bmp");
			iShowBMP(755,474,"Image\\Arrow4.bmp");
			iShowBMP(755,459,"Image\\Arrow5.bmp");
		}
		if(arrow==2)
		{
			iShowBMP(755,519,"Image\\Arrow1.bmp");
			iShowBMP(755,504,"Image\\Arrow2.bmp");
			iShowBMP(755,489,"Image\\Arrow3.bmp");
			iShowBMP(755,474,"Image\\Arrow4.bmp");
		}
		if(arrow==3)
		{
			iShowBMP(755,519,"Image\\Arrow1.bmp");
			iShowBMP(755,504,"Image\\Arrow2.bmp");
			iShowBMP(755,489,"Image\\Arrow3.bmp");
		}
		if(arrow==4)
		{
			iShowBMP(755,519,"Image\\Arrow1.bmp");
			iShowBMP(755,504,"Image\\Arrow2.bmp");
		}
		if(arrow==5)
		{
			iShowBMP(755,519,"Image\\Arrow1.bmp");
		}
		if(arrow==6)
		{
			if((((blackPosition_Y<=214 && blackPosition_Y>=203) || (blackPosition_Y>=305 && blackPosition_Y<=316 )) && (arrowPosition_X1==25)) || (((violetPosition_Y<=316 && violetPosition_Y>=305) || (violetPosition_Y>=226 && violetPosition_Y<=237 )) && (arrowPosition_X1==30)) || (((yellowPosition_Y<=316 && yellowPosition_Y>=305) || (yellowPosition_Y>=249 && violetPosition_Y<=260 )) && (arrowPosition_X1==35)) || (((greenPosition_Y<=316 && greenPosition_Y>=305) || (greenPosition_Y>=271 && greenPosition_Y<=282 )) && (arrowPosition_X1==40)) || (((redPosition_Y>=294 && redPosition_Y<=316 )) && (arrowPosition_X1==50)) || (arrowPosition_X1<=15))
			{
				display=afterGamePage;
				blackPosition_Y=436;
				violetPosition_Y=447;
				yellowPosition_Y=459;			
				greenPosition_Y=470;
				redPosition_Y=481;
				arrowDirection=0;
				arrow=0;
				arrowPosition_X1=770;
				arrowPosition_X2=arrowPosition_X1+60;
				totalScore=0;
				exitDisplay=0;
				score=0;
				if(difficult_==easy)
				{
					minutes1=1;
					seconds1=60;
				}
				else if(difficult_==normal)
				{
					minutes2=0;
					seconds2=50;
				}
				else if(difficult_==hard)
				{
					minutes3=0;
					seconds3=40;
				}
			}
		}

		//iShowBMP(arrowPosition_X,313,"Image\\Arrow1.bmp");

		iSetColor(0,0,0);						//Black Color
		iFilledRectangle(15,blackPosition_Y,7.5,112.5); 

		iSetColor(99,70,156);					//Violet Color
		iFilledRectangle(22.5,violetPosition_Y,7.5,90);

		iSetColor(255,198,10);					//Yellow Color
		iFilledRectangle(30,yellowPosition_Y,7.5,67.5);

		iSetColor(26,177,63);					//Green Color
		iFilledRectangle(37.5,greenPosition_Y,7.5,45);

		iSetColor(255,30,0);					//Red Color
		iFilledRectangle(45,redPosition_Y,7.5,22.5);

		if(exitDisplay==exitPage2)
		{
			
			iShowBMP(372,232,"Image\\EXIT.bmp");
			if(selectedExit==0)
			{
				iSetColor(26,177,63);
				iText(exitX,242,eXIT[0],GLUT_BITMAP_TIMES_ROMAN_24);
			}
			else
			{
				iSetColor(255,30,0);
				iText(exitX,242,eXIT[0]);
			}
			if(selectedExit==1)
			{
				iSetColor(26,177,63);
				iText(exitX+150,242,eXIT[1],GLUT_BITMAP_TIMES_ROMAN_24);}

			else
			{
				iSetColor(255,30,0);
				iText(exitX+190,242,eXIT[1]);
			}
		}

	}

	else if(display==afterGamePage)
	{
		iSetColor(0,0,0);	
		iFilledRectangle(0,0,1000,564);
		iShowBMP(0,100,"Image\\GameOverPage.bmp");

		FILE* hScore=fopen("Documents\\HighScore.txt" , "r");

		fscanf(hScore,"%d",&num);
		fclose(hScore);
		if(num1>=num && num1>0)
		{
			iSetColor(255,255,255);
			iText(350,500,"C O N G R A T U L A T I O N S", GLUT_BITMAP_TIMES_ROMAN_24);

			iSetColor(255,255,255);	
			iText(290,200,"YOU'VE REACHED TO THE HIGH SCORE", GLUT_BITMAP_TIMES_ROMAN_24);
			iText(495,150,score_, GLUT_BITMAP_TIMES_ROMAN_24);

		}

		else
		{
			iSetColor(255,255,255);
			iText(400,200,"Your Score :", GLUT_BITMAP_TIMES_ROMAN_24);
			iText(530,200,score_, GLUT_BITMAP_TIMES_ROMAN_24);
		}
		
		if(selectedPlayOrExit==0)
		{
			iSetColor(75,21,220);
		}
		else
		{
			iSetColor(255,255,255);
		}
		iText(playORexitX,30,playORexit[0],GLUT_BITMAP_TIMES_ROMAN_24);
		if(selectedPlayOrExit==1)
		{
			iSetColor(75,21,220);
		}

		else
		{
			iSetColor(255,255,255);
		}
		iText(playORexitX+677,30,playORexit[1],GLUT_BITMAP_TIMES_ROMAN_24);

		
	}

	else if(display==aboutPage)
	{
		if(credSec>5)
			iShowBMP(0,0,"Image\\AboutPage.bmp");
		else
			iShowBMP(0,0,"Image\\Inspired.bmp");
	}

	else if(display==instructionsPage)
	{
		iShowBMP(0,0,"Image\\InstructionPage.bmp");
	}

	else if(display==highScorePage)
	{
		iShowBMP(0,0,"Image\\HighScore.bmp");

		int textHighScoreLineY=282;
		iSetColor(255,255,255);
		for(int c=0;c<highScoreLenth;c++)
		{
			iText(495,textHighScoreLineY,highScore[c],GLUT_BITMAP_TIMES_ROMAN_24);
			iText(497,textHighScoreLineY,highScore[c],GLUT_BITMAP_TIMES_ROMAN_24);
			textHighScoreLineY-=20;
		}

		iSetColor(255,255,255);
		iText(830,30,"Press F1 to clear");
		iRectangle(825,20,145,25);
	}

	else if(display==difficultModePage)
	{
		iShowBMP(0,0,"Image\\DifficultModePage.bmp");

		if(selectedDifficult==0)
		{
			iSetColor(255,153,10);
		}
		else
		{
			iSetColor(255,255,255);
		}
		iText(250,262,difficult[0],GLUT_BITMAP_TIMES_ROMAN_24);
		if(selectedDifficult==1)
		{
			iSetColor(255,153,10);
		}
		else
		{
			iSetColor(255,255,255);
		}
		iText(450,262,difficult[1],GLUT_BITMAP_TIMES_ROMAN_24);
		if(selectedDifficult==2)
		{
			iSetColor(255,153,10);
		}
		else
		{
			iSetColor(255,255,255);
		}
		iText(650,262,difficult[2],GLUT_BITMAP_TIMES_ROMAN_24);
	}

}

void iMouseMove(int mx, int my)
{
	//place your codes here
}


void iMouse(int button, int state, int mx, int my)
{
	//place your codes here
	if(display==gamePage)
	{
		if(button==GLUT_LEFT_BUTTON && state== GLUT_DOWN)
		{
			arrowDirection=1;
			if(arrowPosition_X1==770)
			arrow++;
		}
	}
	
}

void iKeyboard(unsigned char key)
{
	//place your codes here

	if(key==KEY_ENTER && selectedMenu==i && display==menuPage)
	{
		display=difficultModePage;
	}
	else if(key==KEY_SPACE && display==startPage)
	{
		display=menuPage;
		arrowDirection=0;
		arrow=0;
	}
	else if(key== KEY_ECS && display==menuPage)
	{
		display=startPage;
	}
	else if(key==KEY_ENTER && selectedMenu==m && display==menuPage)
	{
		exit(0);
	}
	else if(key== KEY_ECS && display==gamePage)
	{
		exitDisplay=exitPage2;
	}
	
	else if(key==KEY_ENTER && selectedExit==0 && exitDisplay==exitPage2 && display==gamePage)
	{
		display=menuPage;
		blackPosition_Y=436;
		violetPosition_Y=447;
		yellowPosition_Y=459;			
		greenPosition_Y=470;
		redPosition_Y=481;
		arrowDirection=0;
		arrow=0;
		arrowPosition_X1=770;
		arrowPosition_X2=arrowPosition_X1+60;
		totalScore=0;
		exitDisplay=0;
		score=0;
		if(difficult_==easy)
		{
			minutes1=1;
			seconds1=60;
		}
		else if(difficult_==normal)
		{
			minutes2=0;
			seconds2=50;
		}
		else if(difficult_==hard)
		{
			minutes3=0;
			seconds3=40;
		}
		
	}
	else if(key==KEY_ENTER && selectedExit==1  && exitDisplay==exitPage2)
	{
		exitDisplay=0;
	}
	
	else if(key==KEY_ENTER && selectedMenu==l && display==menuPage)
	{
		display=aboutPage;
	}
	else if(key== KEY_ECS && display==aboutPage)
	{
		display=menuPage;
		credSec=0;
	}
	else if(key==KEY_ENTER && selectedMenu==k && display==menuPage)
	{
		display=instructionsPage;
	}
	else if(key== KEY_ECS && display==instructionsPage)
	{
		display=menuPage;
	}
	else if(key==KEY_ENTER && selectedMenu==j && display==menuPage)
	{
		FILE* fpointer=fopen("Documents\\HighScore.txt" , "r");

		int r=0;
		while(!feof(fpointer))
		{
			fgets(highScore[r], 500, fpointer);
			r++;
			highScoreLenth++;
		}

		fclose(fpointer);

		display=highScorePage;
	}
	else if(key== KEY_ECS && display==highScorePage)
	{
		display=menuPage;
	}
	else if(key== KEY_ECS && display==difficultModePage)
	{
		display=menuPage;
	}
	
	else if(key==KEY_ENTER && selectedPlayOrExit==0 && display==afterGamePage)
	{
		display=gamePage;
		blackPosition_Y=436;
		violetPosition_Y=447;
		yellowPosition_Y=459;			
		greenPosition_Y=470;
		redPosition_Y=481;
		arrowDirection=0;
		arrow=0;
		arrowPosition_X1=770;
		arrowPosition_X2=arrowPosition_X1+60;
		totalScore=0;
		score=0;
	}
	else if(key==KEY_ENTER && selectedPlayOrExit==1 && display==afterGamePage)
	{
		display=menuPage;
		blackPosition_Y=436;
		violetPosition_Y=447;
		yellowPosition_Y=459;			
		greenPosition_Y=470;
		redPosition_Y=481;
		arrowDirection=0;
		arrow=0;
		arrowPosition_X1=770;
		arrowPosition_X2=arrowPosition_X1+60;
		totalScore=0;
		score=0;
	}
	else if(key==KEY_ENTER && selectedDifficult==0 && display==difficultModePage)
	{
		display=gamePage;
		difficult_=easy;
	}
	else if(key==KEY_ENTER && selectedDifficult==1 && display==difficultModePage)
	{
		display=gamePage;
		difficult_=normal;
	}
	else if(key==KEY_ENTER && selectedDifficult==2 && display==difficultModePage)
	{
		display=gamePage;
		difficult_=hard;
	}
}

void iSpecialKeyboard(unsigned char key)
{
	//place your codes for other keys here
	if(key== GLUT_KEY_END && display==startPage)
	{
		exit(0);
	}
	else if(display==menuPage)
	{
		if(key==GLUT_KEY_UP)
		{
			selectedMenu=selectedMenu>0?selectedMenu-1:5-1;
		}
		else if(key==GLUT_KEY_DOWN)
		{
			selectedMenu=(selectedMenu+1)%5;
		}
	}
	else if(display==gamePage && exitDisplay==exitPage2)
	{
		if(key==GLUT_KEY_LEFT)
		{
			selectedExit=selectedExit>0?selectedExit-1:2-1;
		}
		else if(key==GLUT_KEY_RIGHT)
		{
			selectedExit=(selectedExit+1)%2;
		}
	}
	else if(display==afterGamePage)
	{
		if(key==GLUT_KEY_LEFT)
		{
			selectedPlayOrExit=selectedPlayOrExit>0?selectedPlayOrExit-1:2-1;
		}
		else if(key==GLUT_KEY_RIGHT)
		{
			selectedPlayOrExit=(selectedPlayOrExit+1)%2;
		}
	}
	else if(display==difficultModePage)
	{
		if(key==GLUT_KEY_LEFT)
		{
			selectedDifficult=selectedDifficult>0?selectedDifficult-1:3-1;
		}
		else if(key==GLUT_KEY_RIGHT)
		{
			selectedDifficult=(selectedDifficult+1)%3;
		}
	}

	else if(key==GLUT_KEY_F1 && display==highScorePage)
	{
		FILE* hScore=fopen("Documents\\HighScore.txt" , "w");

		fprintf(hScore,"%d",0);
		fclose(hScore);

		FILE* fpointer=fopen("Documents\\HighScore.txt" , "r");

		int r=0;
		while(!feof(fpointer))
		{
			fgets(highScore[r], 500, fpointer);
			r++;
		}
		fclose(fpointer);
	}

	
}

int main()
{
	//place your own initialization codes here.
	PlaySound("Music\\MUSIC",NULL,SND_LOOP | SND_ASYNC);
	iSetTimer(1200,pressSPACE);
	iSetTimer(1,boardMovement);
	iSetTimer(25,arrowShot);
	iSetTimer(1000,stopWatchEasy);
	iSetTimer(1000,CreditMove);
	iInitialize(1000,564,"The Forest Archer");
	return 0;
}